package com.example.perritos2.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.perritos2.R
import com.example.perritos2.adapters.DogAdapter
import com.example.perritos2.databinding.FragmentMainBinding
import com.example.perritos2.http.ApiInterface
import com.example.perritos2.models.Dog
import com.example.perritos2.models.MessageResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainFragment : Fragment(R.layout.fragment_main) {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private var baseURL = "https://dog.ceo/api/"
//    private lateinit var dogRecyclerView : RecyclerView
    private lateinit var dogAdapter: DogAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        dogRecyclerView = findViewById(R.id.recyclerDog)
        binding.recyclerDog.setHasFixedSize(true)
        binding.recyclerDog.layoutManager = LinearLayoutManager(this.activity)
        //val dogs = getData1()
        //setAdapter(dogs)
        getData()
    }

    private fun setAdapter(dogList:MutableList<Dog>){
        dogAdapter = DogAdapter(dogList){ dog ->
            Toast.makeText(this.activity, dog.name, Toast.LENGTH_SHORT).show()
//            findNavController().navigate(R.id.action_mainFragment_to_detailFragment)
        }
        Log.d("DOG","SETTING ADAPTER")
        binding.recyclerDog.adapter = dogAdapter
    }

    fun getData(){
        val lst:MutableList<Dog> = mutableListOf()
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseURL)
            .build()
            .create(ApiInterface::class.java)
        val builder = retrofitBuilder.getDogs()

        Log.d("DOG","getData")
        builder.enqueue(object: Callback<MessageResponse?> {
            override fun onResponse(
                call: Call<MessageResponse?>,
                response: Response<MessageResponse?>
            ) {
                Log.d("DOG","Here")
                val responseBody = response.body()!!
                for(imageUrl in responseBody.message)
                {
                    lst.add(Dog(imageUrl,imageUrl))
                }
                Log.d("DOG","PRE SETTING ADAPTER")
                setAdapter(lst)
            }
            override fun onFailure(call: Call<MessageResponse?>, t: Throwable) {
                Log.d("DOG","Error")
            }
        })
    }
}